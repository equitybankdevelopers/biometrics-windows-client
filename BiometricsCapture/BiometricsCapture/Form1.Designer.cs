﻿namespace BiometricsCapture
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.picPrint = new System.Windows.Forms.PictureBox();
            this.txtBase64Image = new System.Windows.Forms.RichTextBox();
            this.cmdCapture = new System.Windows.Forms.Button();
            this.pbImageQuality = new System.Windows.Forms.ProgressBar();
            this.txtImageQuality = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(302, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Place your finger on the Biometrics device";
            // 
            // picPrint
            // 
            this.picPrint.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.picPrint.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picPrint.Image = global::BiometricsCapture.Properties.Resources.secugen_hamster_iv_fingerprint_scanner_fbi_stqc_certified_250x250;
            this.picPrint.Location = new System.Drawing.Point(26, 71);
            this.picPrint.Name = "picPrint";
            this.picPrint.Size = new System.Drawing.Size(345, 525);
            this.picPrint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picPrint.TabIndex = 1;
            this.picPrint.TabStop = false;
            // 
            // txtBase64Image
            // 
            this.txtBase64Image.Location = new System.Drawing.Point(396, 71);
            this.txtBase64Image.Name = "txtBase64Image";
            this.txtBase64Image.ReadOnly = true;
            this.txtBase64Image.Size = new System.Drawing.Size(1142, 525);
            this.txtBase64Image.TabIndex = 2;
            this.txtBase64Image.Text = "";
            // 
            // cmdCapture
            // 
            this.cmdCapture.Location = new System.Drawing.Point(262, 785);
            this.cmdCapture.Name = "cmdCapture";
            this.cmdCapture.Size = new System.Drawing.Size(973, 87);
            this.cmdCapture.TabIndex = 3;
            this.cmdCapture.Text = "Capture Fingerprint Image";
            this.cmdCapture.UseVisualStyleBackColor = true;
            this.cmdCapture.Click += new System.EventHandler(this.cmdCapture_Click);
            // 
            // pbImageQuality
            // 
            this.pbImageQuality.Location = new System.Drawing.Point(26, 700);
            this.pbImageQuality.Name = "pbImageQuality";
            this.pbImageQuality.Size = new System.Drawing.Size(1512, 56);
            this.pbImageQuality.TabIndex = 4;
            // 
            // txtImageQuality
            // 
            this.txtImageQuality.AutoSize = true;
            this.txtImageQuality.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImageQuality.Location = new System.Drawing.Point(1437, 638);
            this.txtImageQuality.Name = "txtImageQuality";
            this.txtImageQuality.Size = new System.Drawing.Size(101, 36);
            this.txtImageQuality.TabIndex = 5;
            this.txtImageQuality.Text = "100 %";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1550, 896);
            this.Controls.Add(this.txtImageQuality);
            this.Controls.Add(this.pbImageQuality);
            this.Controls.Add(this.cmdCapture);
            this.Controls.Add(this.txtBase64Image);
            this.Controls.Add(this.picPrint);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biometrics Capture";
            ((System.ComponentModel.ISupportInitialize)(this.picPrint)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picPrint;
        private System.Windows.Forms.RichTextBox txtBase64Image;
        private System.Windows.Forms.Button cmdCapture;
        private System.Windows.Forms.ProgressBar pbImageQuality;
        private System.Windows.Forms.Label txtImageQuality;
    }
}

